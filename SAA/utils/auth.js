import React from 'react'
import { AsyncStorage } from 'react-native';

export function useAuth() {
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            isLoading: false,
            user: action.user
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            user: action.user
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            user: null
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      user: null
    }
  );

  React.useEffect(() => {
    let isCanceled = true;
    // Fetch the token from storage then navigate to our appropriate place

      const bootstrapAsync = async () => {
          try {
            user = await AsyncStorage.getItem('user');
          } catch (error) {

          }
          dispatch({ type: 'RESTORE_TOKEN', user: JSON.parse(user) });
      };

      if(isCanceled)
      {
        bootstrapAsync();
      }

    return () => {isCanceled = false}
  }, []);

  const auth = React.useMemo(
    () => ({
      signIn: async data => {
        await AsyncStorage.setItem('user', JSON.stringify(data))

        dispatch({ type: 'SIGN_IN', user: data });
      },
      signOut: async () => {
        await AsyncStorage.multiRemove(['userToken', 'user'])
        dispatch({ type: 'SIGN_OUT' })
      }
    }),
    []
  );

  return { auth, state }
}