import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faHome, faComments, faBookReader, faBars, faUserCog, faPowerOff, faSearch } from '@fortawesome/free-solid-svg-icons'
import { enableScreens } from 'react-native-screens'
import { Text, View, SafeAreaView } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { AuthContext } from '../contexts/authContext'
import { AppContext } from '../contexts/appContext'

import styles from '../Style'
import home from '../components/home'
import OffreScreen from '../components/Offre/OffreScreen'
import OffreDetail from '../components/Offre/OffreDetail'
import Categorie from '../components/Categorie/categorieScreen'
import Forum from '../components/Forum/forumScreen'
import ForumDetail from '../components/Forum/ForumDetail'
import Profile from '../components/Profile'

enableScreens();
const Tab = createBottomTabNavigator();

function Navigator() {
  return (

    <Tab.Navigator tabBarOptions={{ style: { backgroundColor: '#096cad' } }}>

      <Tab.Screen
        name="Acceuil"
        component={AcceuilStackNavigator}
        options={{
          tabBarIcon: (({ focused }) => <FontAwesomeIcon icon={faHome} size={28} style={{ color: focused ? 'white' : '#999999' }} />),
          tabBarLabel: (({ focused }) => focused ? <Text style={styles.labelBottomNavbar}>Acceuil</Text> : null),

        }}
      />

      <Tab.Screen
        name="Offre"
        component={OffreStackNavigator}
        options={{
          tabBarIcon: (({ focused }) => <FontAwesomeIcon icon={faBookReader} size={28} style={{ color: focused ? 'white' : '#999999' }} />),
          tabBarLabel: (({ focused }) => focused ? <Text style={styles.labelBottomNavbar}>Offres</Text> : null),
        }}

      />

      <Tab.Screen
        name="Forum"
        component={ForumStackNavigator}
        options={{
          tabBarIcon: (({ focused }) => <FontAwesomeIcon icon={faComments} size={28} style={{ color: focused ? 'white' : '#999999' }} />),
          tabBarLabel: (({ focused }) => focused ? <Text style={styles.labelBottomNavbar}>Forum</Text> : null)
        }}
      />

    </Tab.Navigator>

  )
}

const OffreStack = createStackNavigator();

function OffreStackNavigator() {
  return (
    <OffreStack.Navigator screenOptions={{ headerTitleStyle: { color: 'white' }, headerTitleAlign: 'center', headerStyle: { backgroundColor: '#096cad'}, headerTintColor: 'white'}}>
      <OffreStack.Screen name="Offres" component={OffreScreen} options={
        ({ navigation }) => ({
          headerLeft: () => (<HeaderButton navigation={navigation}/>),
        })
        }/>
      <OffreStack.Screen name="OffreDetail" component={OffreDetail} options={{title: ''}}/>
    </OffreStack.Navigator>
  )
}

const AcceuilStack = createStackNavigator();

function AcceuilStackNavigator() {
  return (
    <AcceuilStack.Navigator screenOptions={{ headerTitleStyle: { color: 'white' }, headerTitleAlign: 'center', headerStyle: { backgroundColor: '#096cad' }, headerTintColor: 'white'}}>
      <AcceuilStack.Screen name="Acceuil" component={home} options={({ navigation }) => ({headerLeft: () => (<HeaderButton navigation={navigation}/>) })}/>
    </AcceuilStack.Navigator>
  )
}

const ForumStack = createStackNavigator();

function ForumStackNavigator() {
  return (
    <ForumStack.Navigator mode="modal" screenOptions={{ headerTitleStyle: { color: 'white' }, headerTitleAlign: 'center', headerStyle: { backgroundColor: '#096cad' }, headerTintColor: 'white' }}>
      <ForumStack.Screen name="ForumCategorie" component={Categorie} options={({ navigation }) => ({headerLeft: () => (<HeaderButton navigation={navigation}/>), title: 'Catégories des forums' })}/>
      <ForumStack.Screen name="Forum" component={Forum} options={{title: 'Forums'}}/>
      <ForumStack.Screen name="ForumDetail" component={ForumDetail}/>
    </ForumStack.Navigator>
  )
}

const Drawer = createDrawerNavigator();

export function DrawerNavigator() {
  return (
      <Drawer.Navigator drawerContent={props => <CustomDrawerContent {...props} />} drawerContentOptions={{activeTintColor: '#92278F'}}>
        <Drawer.Screen name="Acceuil" component={Navigator} options={{drawerIcon: ({focused}) => <FontAwesomeIcon icon={faHome} size={23} style={{ color: focused ? '#92278F' : '' }}/> }}/>
        <Drawer.Screen name="Profil" component={Profile} options={{drawerIcon: ({focused}) => <FontAwesomeIcon icon={faUserCog} size={23} style={{color: focused ? '#92278F' : ''}}/> }}/>
      </Drawer.Navigator>
  );
}

function HeaderButton({ navigation })
{
  return(
    <TouchableOpacity onPress={() =>  {navigation.openDrawer()}} style={styles.headerButtonDrawer}>
      <Text>
        <FontAwesomeIcon icon={faBars} size={28} style={{ color: 'white' }} />
      </Text>
    </TouchableOpacity>
  )
}

const SearchOffreButton = ({navigation}) => {
  return(
    <TouchableOpacity onPress={() => {navigation.navigate('OffreSearch')}} style={styles.headerButtonRight}>
      <Text>
        <FontAwesomeIcon icon={faSearch} size={28} style={{ color: 'white' }} />
      </Text>
    </TouchableOpacity>
  )
}

function CustomDrawerContent(props) {
  const { signOut } = React.useContext(AuthContext)
  const { user } = React.useContext(AppContext)

  return (
    <SafeAreaView style={{flex: 1}}>
      <DrawerContentScrollView {...props} >
          <View style={styles.headerDrawerContainer}>
            <Text>{user.prenom} {user.nom}</Text>
            <Text style={{color: '#92278F', fontStyle: 'italic'}}>{user.email}</Text>
          </View>
          <DrawerItemList {...props} />
          <View style={styles.bottomDrawerContainer}>
            <DrawerItem label="Déconnexion" onPress={signOut} icon={() => <FontAwesomeIcon icon={faPowerOff} size={23} style={{color: 'red'}}/>}/>
          </View>
      </DrawerContentScrollView>
    </SafeAreaView>
  );
}


