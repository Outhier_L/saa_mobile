const API_TOKEN = "BAfCernmGHPccf5tLYnu3rlYp2c7x4uNRb7OzMoKXratlEBgETw2PWMz9ozW"
const SERVER = "saa.infofil.fr"
const PORT = ""
const PROTOCOL = 'https'

export const getAllUsers = () => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/users?api_token=' + API_TOKEN

  return fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))

}

export const verifyPassword = (password, id) => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/verifyPassword?api_token=' + API_TOKEN

  return fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      id_utilisateur: id,
      password: password,
    }),
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}

export const getAllOffres = () => {
      const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/offres?&api_token=' + API_TOKEN

      console.log("getAllOffres")
      return fetch(url, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      })
        .then(response => response.json())
        .then((res) => { return res })
        .catch(error => console.log(error))
}

export const getOffreById = (id_offre) => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/offres/'+ id_offre + '?api_token=' + API_TOKEN

  console.log("getOffreById")

  return fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}

export const getForumByCategorie = (id_categorie) => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/categories/forums/' + id_categorie + '?api_token=' + API_TOKEN

  console.log("getForumByCategorie")

  return fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}

export const getAllCategories = () => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/categories?api_token=' + API_TOKEN

  console.log("getAllCategories")

  return fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}


export const getForumById = (id_topic) => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/forums/'+ id_topic + '?api_token=' + API_TOKEN

  console.log("getForumById")

  return fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}


export const getAllFromForums = (id_topic) => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/forums/all/'+ id_topic +'?api_token=' + API_TOKEN

  console.log("getAllFromForums")

  return fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}

export const getLastTopicOfCategorie = (id_categorie) => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/categories/lastForum?api_token=' + API_TOKEN

  console.log("getLastTopicOfCategorie")

  return fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      id_categorie: id_categorie,
    }),
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}

export const getResponsesFromTopic = (id_topic) => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/forums/responses/'+ id_topic + '?api_token=' + API_TOKEN

  console.log("getResponsesFromTopic")

  return fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}

export const getUserById = (id_utilisateur) =>{
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/users/'+ id_utilisateur + '?api_token=' + API_TOKEN

  console.log("getUserById")

  return fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}


export const createResponse = (description, topic_id, utilisateur_id) => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/ReponseTopic?api_token=' + API_TOKEN

  console.log("createResponse")
  console.log(utilisateur_id)

  return fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      reponse: description,
      topic_id: topic_id,
      utilisateur_id: utilisateur_id
    }),
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}

export const setResolveTopic = (topic_id, resolve) => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/forums/resolve?api_token=' + API_TOKEN

  console.log("setResolveTopic")

  return fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      topic_id: topic_id,
      cloturer: resolve
    }),
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}

export const checkFavForum = (topic_id, utilisateur_id) => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/forums/checkFav?api_token=' + API_TOKEN

  console.log("checkFavForum")

  return fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      topic_id: topic_id,
      utilisateur_id: utilisateur_id
    }),
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}

export const setFavForum = (topic_id, utilisateur_id) => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/forums/setFav?api_token=' + API_TOKEN

  console.log("setFavForum")

  return fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      topic_id: topic_id,
      utilisateur_id: utilisateur_id
    }),
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}

export const setFavOffre = (offre_id, utilisateur_id) => {
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/offres/setFav?api_token=' + API_TOKEN

  console.log("setFavOffre")

  return fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      offre_id: offre_id,
      utilisateur_id: utilisateur_id
    }),
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}

export const getUserInfoById = (id_utilisateur) =>{
  const url = PROTOCOL + '://' + SERVER + ':' + PORT + '/api/users/infos/'+ id_utilisateur + '?api_token=' + API_TOKEN

  console.log("getUserInfoById")

  return fetch(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  })
    .then(response => response.json())
    .then((res) => { return res })
    .catch(error => console.log(error))
}


export const getPic = (path) => {

  if(path.charAt(0) == "/")
  {
    return PROTOCOL + "://" + SERVER + ':' + PORT + '/storage' + path
  }
  else
  {
    return PROTOCOL + "://" + SERVER + ':' + PORT + '/storage/' + path
  }
}