export const getLocfromAdress = (adresse) => {
    const url = 'https://eu1.locationiq.com/v1/search.php?key=61c799231a29ca&q=' + adresse + '&format=json'

    return fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then((res) => { return res })
      .catch(error => console.log(error))
  }