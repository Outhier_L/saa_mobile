import { StyleSheet, Dimensions  } from "react-native"
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


export default StyleSheet.create({

    container: {
        flex: 1,
        marginTop: 30,
    },

    homeTitle: {
        textAlign: 'center',
        fontSize: 30
    },

    homeBody: {
        fontSize: 20,
        margin: 20,

    },

    centerImage: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    homeImage: {
        width: 200,
        height: 200
    },

    loginContainer: {
        paddingTop: 80,
    },

    formLoginContainer: {
        paddingHorizontal: 20,
        paddingVertical: 5,
    },

    titleLoginContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    titleLogin:{
        fontSize:30
    },

    loginInput: {
        width: '100%',
        backgroundColor: '#e8e8e8',
        marginVertical: 10,
        padding: 20,
        borderRadius: 80
    },

    loginInputInvalid: {
        width: '100%',
        backgroundColor: '#e8e8e8',
        marginVertical: 10,
        padding: 20,
        borderRadius: 80,
        borderColor: 'red',
        borderWidth: 1
    },

    loginButton: {
        backgroundColor: '#dd1576',
        width: '100%',
        borderRadius: 80,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        marginVertical: 32
    },

    loginButtonText: {
        color: 'white',
        fontWeight: '500',
        fontSize: 16,
    },

    loginErrorText: {
        color: 'red',
        fontWeight: 'bold',
        fontSize: 16,
        marginVertical: 20,
        marginHorizontal: 10,
    },

    loginIcon: {
        color: 'white',
        paddingLeft: 20,
        textAlign: 'right'
    },

    //App Container

    AppContainer: {
        flex: 1,
    },

    //loading component

    loadingContainer: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },

    loadingText: {
        paddingTop: 35,
        fontSize: 16,
        fontWeight: 'bold'
    },

    labelBottomNavbar: {
        fontSize: 12,
        fontWeight: 'bold',
        color: 'white'
    },

    //DrawerNavigation

    headerButtonDrawer: {
        justifyContent: 'center',
        paddingLeft: 10
    },

    headerButtonRight: {
        justifyContent: 'center',
        paddingRight: 10
    },

    bottomDrawerContainer: {
        flex: 1,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },

    headerDrawerContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        borderBottomColor: '#f4f4f4',
        borderBottomWidth: 1,
        padding: 10

    },

    //Offre Item on flatList

    offreItemMainContainer: {
        flex: 1,
        marginTop: 8,
        height: 180,
        backgroundColor: 'white'
    },

    OffreItemHeaderContainer: {
        flex: 2.5,
        flexDirection: 'row',
        marginTop: 5,
        marginLeft: 10
    },

    offreItemTitleText: {
        flex: 1,
        flexWrap: 'wrap',
        fontSize: 20,
        fontWeight: 'bold',
        paddingRight: 5
    },

    offreItemIconContainer: {
        marginRight: 20,
    },

    offreItemBodyContainer: {
        flex: 3,
        marginLeft: 18
    },

    offreItemTitleStyle: {
        fontWeight: 'bold',
        textDecorationLine: 'underline'
    },

    offreItemFooterContainer: {
        flex: 3,
        flexDirection: 'row',
    },

    offreItemBadgeStyle: {
        backgroundColor: '#E5E7E9',
        margin: 18,
        marginLeft: 8,
        padding: 3,
        fontWeight: 'bold',
        alignItems: 'flex-start',
        textAlign: 'center',
        borderRadius: 80,

    },

    offreItemDateText:{
        fontSize: 12,
        fontStyle: 'italic',
        color: '#999999',
        position: 'absolute',
        bottom: 18,
        right: 10
    },

    //Offre Detail

    offreDetailContainer: {
        flex: 1,
        padding: 15
    },

    offreDetailHeaderContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    offreDetailTitleText: {
        fontSize: 20,
        fontWeight: 'bold'
    },

    offreDetailInformationContainer: {
        flex: 2,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        
    },

    offreDetailInformationText: {
        fontSize: 16,
        fontWeight: 'bold',
        paddingTop: 10,
        marginRight: 10, 
        marginTop: 20
    },

    offreDetailIcon:{
        marginTop: 3,
        marginRight: 5
    },

    offreDetailDescriptionContainer: {
        flex: 3,
        marginTop: 50
    },

    offreDetailDescriptionText: {

    },

    offreDetailMapContainer: {
        flex: 1,
        backgroundColor: 'red'
    },

    offreDetailMapView: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },

    //button open drawer style

    buttonOpenDrawerContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginBottom: 36,
        marginRight: 20
    },

    buttonOpenDrawer: {
        backgroundColor: '#dd1576',
        padding: 10,
        borderRadius: 80,
        position: 'absolute',
        bottom:0
    },

    //Categorie item on flatlist

    categorieIteamMainContainer: {
        marginTop: 8,
        height: 130,
        backgroundColor: 'white',
        flexDirection: "row"
    },

    categorieItemPic: {
        width: 100,
        height: 100,
        margin: 5,
        marginTop: 15,
        backgroundColor: 'gray',
        justifyContent: 'center',
        alignItems: 'center'
    },

    categorieItemContent: {
        flex: 1,
        margin: 5
    },

    categorieItemHeaderContainer: {
        flex: 3,
        flexDirection: 'row'
    },

    categorieItemTitleText: {
        flex: 1,
        flexWrap: 'wrap',
        fontSize: 20,
        fontWeight: 'bold',
    },

    categorieItemLastTopicMainContainer: {
        flex: 3,
    },

    categorieItemLastTopicContainer: {
        flex: 1,
    },

    categorieItemLastTopicTitleText: {
        fontSize: 12,
    },

    categorieItemLastTopicFooterContainer: {
        flex: 1
    },

    categorieItemLastTopicFooterText: {
        fontSize: 12,
        fontStyle: 'italic',
        color: '#999999',
        position: 'absolute',
        bottom: 10,
        right: 10
    },

    //Forum item on flatlist

    forumItemMainContainer: {
        flex: 1,
        marginTop: 8,
        height: 80,
        backgroundColor: 'white'
    },

    forumItemHeaderContainer: {
        flex: 2.5,
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 5,
        marginLeft: 10
    },

    forumItemTitleText: {
        flex: 1,
        fontSize: 16,
        fontWeight: 'bold',
        paddingRight: 5
    },

    forumItemFooterContainer: {
        flex: 2,
        flexDirection: 'row',
    },

    forumItemDateContainer: {
        flex: 1,
    },

    forumItemDateText:{
        fontSize: 12,
        fontStyle: 'italic',
        color: '#999999',
        position: 'absolute',
        bottom: 12,
        right: 10,
    },

    forumItemBadgeContainer: {
        flex: 2,
        flexDirection: 'row',
    },

    forumItemBadgeStyle:{
        padding: 3,
        borderRadius: 80,
        fontWeight: 'bold',
        marginLeft: 10,
        marginBottom: 5,
        backgroundColor: '#99c20d',
    },

    forumItemBadgeBadStyle:{
        padding: 3,
        borderRadius: 80,
        fontWeight: 'bold',
        marginLeft: 10,
        marginBottom: 5,
        backgroundColor: '#E74C3C',
    },

    //Forum detail syle

    forumDetailMainContainer: {
        flex: 1,
    },

    forumDetailHeaderContainer: {
        flex: 1,
        margin: 5,
        flexWrap: 'wrap',
        justifyContent: "center",
        alignItems: "center"
    },

    forumDetailHeaderTitleText: {
        flex: 1,
        fontSize: 20,
        fontWeight: 'bold',
        paddingRight: 5,
        justifyContent: "center",
        alignItems: "center"
    },

    forumDetailMainCardContainer: {
        flex: 3,
        backgroundColor: '#f0f8ff',
        borderRadius: 15,
        borderTopLeftRadius: 0,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 0.5
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        marginTop: 20
    },

    forumDetailCardHeaderContainer: {
        flex: 1,
        flexDirection: "row",
        padding: 5,
        borderBottomWidth: 0.5,
        borderBottomColor: 'black',
        flexWrap: 'wrap',
        justifyContent: "center",
        alignItems: "center"
    },

    forumDetailCardUserText: {
        flex: 1,
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 10
    },

    forumDetailCardDateForumText: {
        flex: 1,
        fontSize: 14,
        color: 'gray',
        alignItems: "flex-end",
        textAlign: "right",
        marginRight: 5
    },

    forumDetailCardDescriptionContainer: {
        flex: 3,
        margin: 5,
        padding: 10
    },

    forumDetailCardDescriptionText: {
        fontSize: 16
    },


    responseItemMainCardContainerBlue: {
        flex: 3,
        backgroundColor: 'white',
        marginTop: 20
    },

    responseItemCardHeaderContainer: {
        flex: 1,
        flexDirection: "row",
        padding: 5,
        borderBottomWidth: 0.5,
        borderBottomColor: 'black',
        flexWrap: 'wrap',
        justifyContent: "center",
        alignItems: "center"
    },

    responseItemCardUserTextBlue: {
        flex: 1,
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 10
    },

    responseItemCardDateForumTextBlue: {
        flex: 1,
        fontSize: 14,
        color: 'gray',
        marginRight: 5,
        alignItems: "flex-end",
        textAlign: "right",
    },

    responseItemCardDescriptionContainer: {
        flex: 3,
        margin: 5,
        padding: 10
    },

    responseItemCardDescriptionText: {
        fontSize: 16
    },

    responseItemMainCardContainerWhite: {
        flex: 3,
        backgroundColor: '#f0f8ff',
        marginTop: 20
    },

      responseItemCardUserTextWhite: {
        flex: 1,
        fontSize: 16,
        fontWeight: 'bold',
        marginLeft: 10
    },

    responseItemCardDateForumText: {
        flex: 1,
        fontSize: 14,
        color: 'gray',
        alignItems: "flex-end",
        textAlign: "right",
        marginRight: 5
    },


    //  Button reply

    headerButtonReply: {
        justifyContent: 'center',
        paddingRight: 10
    },

    //Modal response to Topics

    MainModalContainer: {
        margin: 20,
        backgroundColor: "white",
        width: windowWidth/1.2,
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        justifyContent: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },

    CenteredContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },

    //Button Modal

    ModalCloseButton: {
        right: -10,
        top: -10,
        padding: 2,
        position: 'absolute'
    },

    ModalInputResponse: {
        flexDirection: "row",
        width: windowWidth/1.5,
        backgroundColor: '#F8F8F8',
        marginVertical: 20,
        borderRadius: 10,
        padding: 15,
        fontSize: 16
    },

    ModalInputResponseInvalid: {
        flexDirection: "row",
        width: windowWidth/1.5,
        backgroundColor: '#F8F8F8',
        marginVertical: 20,
        borderRadius: 10,
        padding: 15,
        fontSize: 16,
        borderColor: 'red',
        borderWidth: 1
    },

    ModalButtonSendResponse: {
        flexDirection: "row",
        backgroundColor: '#dd1576',
        borderRadius: 80,
        padding: 15,
        marginVertical: 22,
    },

    ModalButtonSendResponseContainer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
    },

    ModalButtonSendResponseText: {
        flex: 11,
        color: 'white',
        fontSize: 18,
    },

    ModalButtonSendResponseIcon: {
        flex: 1,
        alignItems: "flex-end"
    },


    ModalButtonYes: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: 'green',
        borderRadius: 80,
        paddingHorizontal: 30,
        paddingVertical: 10,
        margin: 22,
    },

    ModalButtonNo: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: 'red',
        borderRadius: 80,
        paddingHorizontal: 30,
        paddingVertical: 10,
        margin: 22
    },

    ModalResolveText: {
        flexDirection: 'row',
        fontSize: 16
    },

    ModalResolveButtonText: {
        fontSize: 16,
        fontWeight: "bold"
    },

    ProfileContainer : {
        flex: 1,
    },

    ProfileMainContainer: {
        flex: 1,
        borderRadius: 15,
        margin: 20,
        marginTop: 50,
       alignItems: 'center',
       justifyContent: 'center',
       padding: 30
    },

    ProfileHeaderContainer: {
        flexDirection: 'row',
    },

    ProfileHeaderText: {
        fontSize: 18,
        fontWeight: 'bold'
    },

    ProfileContainContainer: {
        marginTop: 10
    },

    ProfileContainerText: {
        padding: 8,
        fontSize: 14,
    },

    ProfileInfosMainContainer: {
        flex:1,
        flexDirection: 'row'
    },

    ProfileInfosContainer: {
        flex: 1,
        borderRadius: 15,
        backgroundColor: 'red',
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 0.1
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        margin: 10,
       alignItems: 'center',
       justifyContent: 'center',
       padding: 20,
    },

    ProfileInfosText: {
        fontSize: 14,
        fontWeight: 'bold',
        color: 'white'
    },

    ProfileInfosTextNumber: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'white',
        marginTop: 10
    },

    yellowBackground: {
        backgroundColor: '#F1C40F',
    },

    blueBackground: {
        backgroundColor: '#096cad',
    },

    purpleBackGround: {
        backgroundColor: '#dd1576',
    },

    SearchScreenMainContainer: {
        flex: 1
    },

    SearScreenHeaderContainer: {
        flex: 1,
        flexDirection: 'row'
    }

})