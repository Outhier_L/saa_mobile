import React from 'react';
import { NavigationContainer } from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack';
import {  useAuth } from './utils/auth'
import { AuthContext } from './contexts/authContext'
import loginForm from './components/Auth/Login'
import loading from './components/loading/loading'
import { DrawerNavigator } from './navigation/Navigation'
import { AppContext } from './contexts/appContext'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';

export default function App()  {

  const { auth, state } = useAuth();

  const AuthStack = createStackNavigator();

  function renderScreens() {

    return (
          state.isLoading ? (
              <AuthStack.Screen name="Splash" component={loading} />

            ) : (state.userToken !== null && state.user !== null) ? (
              <AuthStack.Screen name="nav">
              {() => (
                  <AppContext.Provider value={state.user}>
                    <DrawerNavigator />
                  </AppContext.Provider>
              )}
              </AuthStack.Screen>
          ) :
            (
              <AuthStack.Screen name="SignIn" component={loginForm} />
            )
    )
  }

  return (
    <AuthContext.Provider value={auth}>
      <SafeAreaProvider>
        <NavigationContainer>
          <AuthStack.Navigator screenOptions={{ gestureEnabled: false, headerShown: false }}>
            {renderScreens()}
          </AuthStack.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
    </AuthContext.Provider>
  )
}

