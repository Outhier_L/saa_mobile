import React from 'react'
import { View, Text, TouchableOpacity, ScrollView} from 'react-native'
import moment from 'moment';
import 'moment/locale/fr';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faHeart as farHeart} from '@fortawesome/free-regular-svg-icons'
import { faHeart as fasHeart }  from '@fortawesome/free-solid-svg-icons'
import styles from '../../Style'
import { AppContext } from '../../contexts/appContext';

const ForumItem = (props) => {

    const forum = props.forum
    const displayDetailForForum = props.displayDetailForForum
    const setFav = props.setFav
    const { user } = React.useContext(AppContext)
    moment.locale('fr')

    function DisplayResolu()
    {
        if(forum.cloturer ==  '1')
        {
            return (
                <Text style={styles.forumItemBadgeStyle}>Résolu</Text>
            )
        }
        else if(forum.cloturer == '0')
        {
            return (
                <Text style={styles.forumItemBadgeBadStyle}>Non résolu</Text>
            )
        }
        return null
    }

    const DisplayFav = () => {
        if(forum.favoris_topic.some(obj => obj.id_utilisateur == user.id_utilisateur))
        {
            return (
                <Text><FontAwesomeIcon icon={fasHeart} size={28} style={{color: 'red'}}></FontAwesomeIcon></Text>
            )
        }
        else
        {
            return (
                <Text><FontAwesomeIcon icon={farHeart} size={28} style={{color: 'red'}}></FontAwesomeIcon></Text>
            )
        }
        return null;
    }

    return (
        <ScrollView style={{backgroundColor: '#E5E7E9'}}>
            <TouchableOpacity onPress={() => displayDetailForForum(forum.id_topic)} style={styles.forumItemMainContainer}>
                <View style={styles.forumItemHeaderContainer}>
                    <Text style={styles.forumItemTitleText}>{forum.titre}</Text>
                    <TouchableOpacity style={styles.offreItemIconContainer} onPress={() => setFav(forum.id_topic)}>
                        <DisplayFav></DisplayFav>
                    </TouchableOpacity>
                </View>
                <View style={styles.forumItemFooterContainer}>
                    <View style={styles.forumItemBadgeContainer}>
                        <DisplayResolu></DisplayResolu>
                    </View>
                    <View style={styles.forumItemDateContainer}>
                        <Text style={styles.forumItemDateText}>{moment(forum.created_at).startOf('second').fromNow()} </Text>
                    </View>
                </View>
            </TouchableOpacity>
        </ScrollView>
    )
}

export default ForumItem