import React from 'react'
import { View, Text } from 'react-native'
import styles from '../../Style'
import moment from 'moment';
import 'moment/locale/fr';

const ResponseItem = (props) => {

    const reponse = props.reponse;

    return (
        <View style={styles.responseItemMainCardContainerBlue}>
            <View style={styles.responseItemCardHeaderContainer}>
                <Text style={styles.responseItemCardUserTextBlue}>{reponse.user.prenom} {reponse.user.nom}</Text>
                <Text style={styles.responseItemCardDateForumTextBlue}>{moment(reponse.created_at).format('LL LTS')}</Text>
            </View>
            <View style={styles.responseItemCardDescriptionContainer}>
                <Text style={styles.responseItemCardDescriptionText}>{reponse.description}</Text>
            </View>
        </View>
    )

}


export default ResponseItem;