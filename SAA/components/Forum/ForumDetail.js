import React, { useEffect, useState } from 'react'
import { View, Text, FlatList, TouchableOpacity, Modal, ActivityIndicator } from 'react-native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faHeart as farHeart} from '@fortawesome/free-regular-svg-icons'
import { faHeart as fasHeart }  from '@fortawesome/free-solid-svg-icons'
import { AppContext } from '../../contexts/appContext'
import moment from 'moment';
import 'moment/locale/fr';
import { getAllFromForums, createResponse, setResolveTopic, setFavForum} from '../../Api/SAA_Api'
import _displayLoading from '../loading/loadingComponent'
import styles from '../../Style'
import ResponseItem from "./ResponseItem"
import toast from '../button/Toast'
import { faCheck, faReply, faTimes } from '@fortawesome/free-solid-svg-icons';
import { TextInput } from 'react-native-gesture-handler';
import { faPaperPlane, faTimesCircle } from '@fortawesome/free-regular-svg-icons';


const ForumDetail = ({ route, navigation }) => {
    const { id_topic } = route.params
    const [forum, setForum] = useState();
    const { user } = React.useContext(AppContext)
    const [isLoading, setIsLoading] = useState(true);
    const [showModal, setshowModal] = useState(false);
    const [showResolveModal, setshowResolveModal] = useState(false)
    const [error, seterror] = useState("")
    const [loading, setloading] = useState(false)
    const [isRefreshing, setIsRefreshing] = useState(false);
    var description = ""
    moment.locale('fr');

    useEffect(() => {
        let isMounted = true; // note this

        if(isMounted)
        {
            _loadForum();
        }
        return () => { isMounted = false };
    }, [])

    const onRefresh = React.useCallback(async () => {
        _loadForum();
    })


    function _loadForum() {
        const set = (data) => {
            setForum(data)
            setIsLoading(false)
        }

        getAllFromForums(id_topic).then(data => set(data))
    }

    const ReplyButton = () => {
        return (
            <TouchableOpacity style={styles.headerButtonReply} onPress={() => (setshowModal(true))}>
                <Text>
                    <FontAwesomeIcon icon={faReply} size={28} style={{ color: 'white' }} />
                </Text>
            </TouchableOpacity>
        )
    }

    const ResolveButton = () => {
        if (forum != undefined) {
            if(forum.utilisateur_id == user.id_utilisateur)
            {
                if (forum.cloturer == true) {
                    return (
                        <TouchableOpacity style={styles.headerButtonReply} onPress={() => (setshowResolveModal(true))}>
                            <Text>
                                <FontAwesomeIcon icon={faTimes} size={28} style={{ color: 'white' }} />
                            </Text>
                        </TouchableOpacity>
                    )
                }
                else {
                    return (
                        <TouchableOpacity style={styles.headerButtonReply}>
                            <Text>
                                <FontAwesomeIcon icon={faCheck} size={28} style={{ color: 'white' }} onPress={() => (setshowResolveModal(true))}/>
                            </Text>
                        </TouchableOpacity>
                    )
                }
            }
        }
        return null;
    }

    const FavButton = () => {
        if(forum != undefined)
        {
            if(forum.favoris_topic.some(obj => obj.id_utilisateur == user.id_utilisateur))
            {
                return (
                    <TouchableOpacity style={styles.headerButtonReply} onPress={() => (_setFav())}>
                      <Text><FontAwesomeIcon icon={fasHeart} size={28} style={{color: 'white'}}></FontAwesomeIcon></Text>
                    </TouchableOpacity>
                )
            }
            else
            {
                return (
                    <TouchableOpacity style={styles.headerButtonReply} onPress={() => (_setFav())}>
                        <Text><FontAwesomeIcon icon={farHeart} size={28} style={{color: 'white'}}></FontAwesomeIcon></Text>
                    </TouchableOpacity>
                )
            }
        }
        return null;
    }

    const _setFav = () => {
        setFavForum(id_topic, user.id_utilisateur).then(data => {
            if(data.fav)
            {
                onRefresh()
                toast('Forum ajouté aux favoris !')
            }
            else
            {
                onRefresh()
                toast('Forum supprimé des favoris !')
            }
        })
    }

    const IconButton = () => {
        navigation.setOptions({
            title: '',
            headerRight: () => (
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <ResolveButton />
                    <FavButton />
                    <ReplyButton />
                </View>
            )
        })

        return null
    }

    const sendResponse = () => {
        setloading(true)
        if (description.length > 0) {
            if (description.length > 3) {
                createResponse(description, id_topic, user.id_utilisateur).then(data => {
                    if (data.create) {
                        setshowModal(false)
                        onRefresh();
                        seterror('')
                    }
                });
            }
            else {
                seterror("La réponse doit être supérieur à 3 caractères !")
                setloading(false)
            }
        }
        else {
            seterror("La réponse ne peut pas être vide !")
            setloading(false)
        }

        setloading(false)
    }

    const ResolveForum = () => {
        setResolveTopic(id_topic, !forum.cloturer).then(data => {
            if(data.cloturer == true)
            {
                setshowResolveModal(false)
                onRefresh();
            }
        })
    }

    const _DisplayLoadingButton = () => {
        if (loading == false) {
            return (
                <Text style={styles.ModalButtonSendResponseIcon}>
                    <FontAwesomeIcon icon={faPaperPlane} size={20} style={{ color: 'white' }} />
                </Text>
            )
        }

        return (
            <View style={styles.ModalButtonSendResponseIcon}>
                <ActivityIndicator size="small" color="white" />
            </View>
        )
    }

    const _DisplayError = () => {
        if (error.length > 0) {
            return (
                <Text style={styles.loginErrorText}>{error}</Text>
            )
        }
        return null;
    }

    const changeDescription = (text) => {
        description = text;
    }

    const _ResponseModal = () => {
        return (
            <View style={styles.CenteredContainer}>
                <Modal animationType="slide" transparent={true} visible={showModal} onRequestClose={() => { setshowModal(!showModal) }}>
                    <View style={styles.CenteredContainer}>
                        <View style={styles.MainModalContainer}>
                            <_DisplayError />
                            <TextInput multiline={true} numberOfLines={3}
                                placeholder="Entrer une réponse..."
                                style={[(error.length > 0) ? styles.ModalInputResponseInvalid : styles.ModalInputResponse]}
                                onChangeText={(text) => changeDescription(text)}
                            />
                            <TouchableOpacity style={styles.ModalButtonSendResponse} onPress={() => (sendResponse())}>
                                <View style={styles.ModalButtonSendResponseContainer}>
                                    <Text style={styles.ModalButtonSendResponseText}> Répondre </Text>
                                    <_DisplayLoadingButton />
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => { setshowModal(!showModal) }} style={styles.ModalCloseButton}>
                                <Text>
                                    <FontAwesomeIcon icon={faTimesCircle} size={32} style={{ color: 'red' }} />
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }

    const _ResolveModal = () => {

        var text = "";

        if(forum != undefined)
        {
            if(forum.cloturer == true)
            {
                text = "Voulez vous décloturer le forum '" + forum.titre + "' ?"
            }
            else
            {
                text = "Voulez vous cloturer le forum '" + forum.titre + "' ?"
            }

            return(
                <View style={styles.CenteredContainer}>
                <Modal animationType="slide" transparent={true} visible={showResolveModal} onRequestClose={() => { setshowResolveModal(!showResolveModal) }}>
                    <View style={styles.CenteredContainer}>
                        <View style={styles.MainModalContainer}>
                            <Text style={styles.ModalResolveText}>{text}</Text>

                            <View style={{flexDirection: 'row'}}>
                                <TouchableOpacity style={styles.ModalButtonNo} onPress={() => {setshowResolveModal(!showResolveModal)}}>
                                    <Text style={styles.ModalResolveText}>Non</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.ModalButtonYes} onPress={() => {ResolveForum()}}>
                                    <Text style={styles.ModalResolveText}>Oui</Text>
                                </TouchableOpacity>
                            </View>

                            <TouchableOpacity onPress={() => { setshowResolveModal(!showResolveModal) }} style={styles.ModalCloseButton}>
                                <Text>
                                    <FontAwesomeIcon icon={faTimesCircle} size={32} style={{ color: 'red' }} />
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
            )
        }
        return null
    }


    function _DisplayTopic() {
        if (forum != undefined) {
            return (
                <View style={styles.forumDetailMainContainer}>
                    <View style={styles.forumDetailHeaderContainer}>
                        <Text style={styles.forumDetailHeaderTitleText}>{forum.titre}</Text>
                    </View>
                    <View style={styles.forumDetailMainCardContainer}>
                        <View style={styles.forumDetailCardHeaderContainer}>
                            <Text style={styles.forumDetailCardUserText}>{forum.user.prenom} {forum.user.nom}</Text>
                            <Text style={styles.forumDetailCardDateForumText}>{moment(forum.created_at).format('LL LTS')}</Text>
                        </View>
                        <View style={styles.forumDetailCardDescriptionContainer}>
                            <Text style={styles.forumDetailCardDescriptionText}>{forum.description}</Text>
                        </View>
                    </View>
                </View>
            )
        }
        return null
    }

    const List = () =>{
        if(forum != undefined)
        {
            return(
                <FlatList data={forum.reponses_topic} keyExtractor={(item) => item.created_at.toString()} refreshing={isRefreshing} onRefresh={() => onRefresh()} renderItem={({ item }) => <ResponseItem reponse={item} />} ListHeaderComponent={<_DisplayTopic></_DisplayTopic>}/>
            )
        }
        return null;
    }

    return (
        <View style={styles.CenteredContainer}>
            <List />
            <_ResponseModal />
            <_ResolveModal />
            <_displayLoading isLoading={isLoading} text='Chargement du forum' size="large" />
            <IconButton></IconButton>
        </View>
    )
}

export default ForumDetail