import React from 'react'
import { SafeAreaView, FlatList, View, TouchableOpacity, Text } from 'react-native';
import { getForumByCategorie, setFavForum} from '../../Api/SAA_Api'
import styles from '../../Style'
import ForumItem from '../Forum/ForumItem'
import _displayLoading from '../loading/loadingComponent'
import { AppContext } from '../../contexts/appContext';
import { useFocusEffect } from '@react-navigation/native';
import toast from '../button/Toast';
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faFilter } from "@fortawesome/free-solid-svg-icons";



const Forum = ({navigation, route}) => {
    const [forumList, setForumList] = React.useState()
    const {id_categorie} = route.params
    const [isRefreshing, setIsRefreshing] = React.useState(false);
    const [isLoading, setIsLoading] = React.useState(true);
    const { user } = React.useContext(AppContext)
    const [filter, setfilter] = React.useState(false)
    const [color, setcolor] = React.useState("white");

    useFocusEffect(
        React.useCallback(() => {
            setIsRefreshing(true)
            _forumList()
            setIsRefreshing(!setIsRefreshing)
        },[])
    )

    const onRefresh = React.useCallback(async() => {
        setIsRefreshing(true)
        _forumList()
        setIsRefreshing(false)
    })

    const _diplayDetailForForum = (id_topic) =>
    {
        navigation.navigate('ForumDetail', {id_topic: id_topic})
    }

    function _forumList()
    {
        const set = (data) =>{
            setForumList(data.forumList)
            setIsLoading(false)
            setfilter(false), setcolor('white');
        }
        getForumByCategorie(id_categorie).then(data => set(data))
    }

    const _setFav = (id_topic) =>{
        setFavForum(id_topic, user.id_utilisateur).then(data => {
            if(data.fav)
            {
                onRefresh();
                toast('Forum ajouté aux favoris !')
            }
            else
            {
                onRefresh()
                toast('Forum supprimé des favoris !')
            }
        })
    }

    const _filter = () => {

        if(filter)
        {
            setfilter(false);
            setcolor('white');
            _forumList();
        }
        else
        {
            setfilter(true);
            setcolor('#92278F');
            setForumList(forumList.filter(e => {
                return e.favoris_topic.some(o => o.id_utilisateur == user.id_utilisateur)
            }));
    
        }
      }
    
      const IconHeader = () => {
        navigation.setOptions({
            headerRight: () => (
                 <TouchableOpacity
                 style={styles.headerButtonReply}
                 onPress={_filter}
                >
            <Text>
              <FontAwesomeIcon
                icon={faFilter}
                size={28}
                style={{ color: color }}
              ></FontAwesomeIcon>
            </Text>
          </TouchableOpacity>
            )
        })
    
        return null
    }

    return(
        <SafeAreaView style={styles.AppContainer}>
            <View>
                <FlatList data={forumList} keyExtractor={(item) => item.id_topic.toString() }  refreshing={isRefreshing} onRefresh={onRefresh} renderItem={({item}) => <ForumItem forum={item} displayDetailForForum={_diplayDetailForForum} setFav={_setFav} />} />
            </View>
            <_displayLoading isLoading={isLoading} text='Chargement des Forums' size="large"/>
            <IconHeader />
        </SafeAreaView>
    )

}

export default Forum;