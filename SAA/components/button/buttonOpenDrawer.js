import React from 'react'
import {TouchableOpacity, Text, View } from 'react-native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAlignJustify } from '@fortawesome/free-solid-svg-icons'

import styles from '../../Style'

const ButtonOpenDrawer = ({navigation}) =>
{
    return(
        <View style={styles.buttonOpenDrawerContainer}>
            <TouchableOpacity style={styles.buttonOpenDrawer} onPress={() =>  {navigation.openDrawer()}}>
                <Text>
                    <FontAwesomeIcon icon={faAlignJustify} size={30}></FontAwesomeIcon>
                </Text>
            </TouchableOpacity>
        </View>
    )
}

export default ButtonOpenDrawer