import React from 'react'
import {ScrollView, View, Text, Image} from 'react-native'

import styles from '../Style'

function Home() {

        return (

            <ScrollView style={styles.container}>
                <Text style={styles.homeTitle}>Bienvenue </Text>
                <View  style={styles.centerImage} >
                    <Image source={require('../assets/icon_saa.png')} style={styles.homeImage}/>
                </View>
                <Text style={styles.homeBody}>Bienvenue sur l'appication des anciens élèves d'ASI, vous pouvez ici regarder les offres et les topics de l'application. </Text>
            </ScrollView>
        )
}

export default Home