import React from 'react'
import { View, ActivityIndicator } from 'react-native'

import styles from '../../Style'

function loading()
{
    return(
        <View style={styles.loadingContainer}>
            <ActivityIndicator size="large" color="#dd1576" />
        </View>
    )
}


export default loading