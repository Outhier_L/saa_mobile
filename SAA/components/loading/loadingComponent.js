import React from 'react'
import { View, ActivityIndicator, Text } from 'react-native'

import styles from '../../Style'

function _displayLoading(props)
{
    if (props.isLoading && props.text && props.size !== null )
    {
        return(
            <View style={styles.loadingContainer}>
                <ActivityIndicator size={props.size} color="#dd1576" />
                <Text style={styles.loadingText}>{props.text}...</Text>
            </View>
        )
    }
    return null
}

export default _displayLoading