import React, { useState } from 'react'
import { View, TextInput, TouchableOpacity, Text, ScrollView, ActivityIndicator } from 'react-native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faSignInAlt } from '@fortawesome/free-solid-svg-icons'
import { AuthContext } from '../../contexts/authContext'

import { getAllUsers, verifyPassword } from '../../Api/SAA_Api'
import styles from '../../Style'

function loginForm() {
    const { signIn } = React.useContext(AuthContext)

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [usersList, setUsersList] = useState();

    function _loadUsers(){
        getAllUsers().then(data => setUsersList(data))
    }

    function _displayLoading()
    {
        if(isLoading)
        {
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size="large" color="white" />
                </View>
            )
        }
        else
         {
             return (
                <Text>
                    <Text style={styles.loginButtonText}>Connexion </Text>
                    <FontAwesomeIcon icon={faSignInAlt} size={15} style={styles.loginIcon} />
                </Text>
             )
         }
    }

    React.useEffect(() => {
        _loadUsers()
    }, [])

    function _verifyAccount() {
        setIsLoading(true)
        var user = null;
        if (email.length > 0 && password.length > 0) {
            for (var u of usersList) {
                if (u.email == email) {
                    user = u;
                }
            }

            if (user !== null) {
                verifyPassword(password, user.id_utilisateur).then(data => {
                    if (data.login == true) {

                        if(user.password_changed)
                        {
                            console.log(user)
                            if(user.active)
                            {
                                signIn({user});
                            }
                            else
                            {
                                setError('Ce compte est désactivé !')
                                setIsLoading(false)
                            }
                        }
                        else
                        {
                            setError('Première connexion veuillez vous connectez à l\'application web pour le changer !')
                            setIsLoading(false)
                        }
                    }
                    else if (data.login == false) {
                        setError('Mot de passe incorrect !')
                        setIsLoading(false)
                    }
                })

            }
            else {
                setError('Aucun compte trouvé pour cette adresse mail !')
                setIsLoading(false)
            }
        }
        else {
            setError('Un des champs est vide !')
            setIsLoading(false)
        }
    }

    return (
        <ScrollView style={styles.loginContainer}>
            <View style={styles.titleLoginContainer}>
                <Text style={styles.titleLogin}>Page de Connexion</Text>
                <Text style={styles.loginErrorText}>{error}</Text>
            </View>
            <View style={styles.formLoginContainer}>

                <TextInput placeholder="Adresse mail"
                    style={[(error.length > 0) ? styles.loginInputInvalid : styles.loginInput]}
                    onChangeText={(email) => { setEmail(email) }}
                    keyboardType={'email-address'}
                    onSubmitEditing={() => _verifyAccount()}
                    autoCapitalize="none"
                />

                <TextInput secureTextEntry={true}
                    placeholder="Mot de passe"
                    style={[(error.length > 0) ? styles.loginInputInvalid : styles.loginInput]}
                    onChangeText={(password) => { setPassword(password) }}
                    onSubmitEditing={() => _verifyAccount()}
                />

                <TouchableOpacity onPress={() => _verifyAccount()} style={styles.loginButton} disabled={isLoading ? true : false}>
                    {_displayLoading()}
                </TouchableOpacity>
            </View>
        </ScrollView>
    )
}
export default loginForm