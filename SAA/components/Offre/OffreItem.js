import React from 'react'
import { View, Text, TouchableOpacity, ScrollView} from 'react-native'
import moment from 'moment';
import 'moment/locale/fr';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faHeart as farHeart} from '@fortawesome/free-regular-svg-icons'
import { faHeart as fasHeart }  from '@fortawesome/free-solid-svg-icons'
import { AppContext } from '../../contexts/appContext';
import styles from '../../Style'

const OffreItem = (props) => {

    const offre = props.offre
    const displayDetailForOffre = props.displayDetailForOffre
    const setFav = props.setFav
    const { user } = React.useContext(AppContext)
    moment.locale('fr')

    const DisplayFav = () => {
        if(offre.favoris_offre.some(obj => obj.id_utilisateur == user.id_utilisateur))
        {
            return (
                <Text><FontAwesomeIcon icon={fasHeart} size={28} style={{color: 'red'}}></FontAwesomeIcon></Text>
            )
        }
        else
        {
            return (
                <Text><FontAwesomeIcon icon={farHeart} size={28} style={{color: 'red'}}></FontAwesomeIcon></Text>
            )
        }
        return null;
    }


    return (
        <ScrollView style={{backgroundColor: '#E5E7E9'}}>
            <TouchableOpacity style={styles.offreItemMainContainer} onPress={() => displayDetailForOffre(offre.id_offre)}>
                <View style={styles.OffreItemHeaderContainer}>
                    <Text style={styles.offreItemTitleText}>{offre.titre}</Text>
                    <TouchableOpacity style={styles.offreItemIconContainer} onPress={() => setFav(offre.id_offre)}>
                        <DisplayFav />
                    </TouchableOpacity>
                </View>
                <View style={styles.offreItemBodyContainer}>
                    <Text><Text style={styles.offreItemTitleStyle}>Entreprise</Text> : {offre.entreprise}</Text>
                    <Text><Text style={styles.offreItemTitleStyle}>Ville</Text> : {offre.ville} ({offre.code_postal})</Text>
                </View>
                <View style={styles.offreItemFooterContainer}>
                    <Text style={styles.offreItemBadgeStyle}>{offre.salaire} par mois</Text>
                    <Text style={styles.offreItemBadgeStyle}>{offre.niveau_etude}</Text>
                    <Text style={styles.offreItemDateText}>{moment(offre.created_at).format('L')}</Text>
                </View>
            </TouchableOpacity>
        </ScrollView>
    )
}

export default OffreItem