import React, { useEffect, useState, useCallback } from 'react'
import { View, Text, ScrollView, RefreshControl, TouchableOpacity } from 'react-native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faBuilding, faGraduationCap, faEuroSign, faPhone, faEnvelope, faCity } from '@fortawesome/free-solid-svg-icons'
import { faHeart as farHeart } from '@fortawesome/free-regular-svg-icons'
import { faHeart as fasHeart } from '@fortawesome/free-solid-svg-icons'
import { getOffreById, setFavOffre } from '../../Api/SAA_Api'
import { AppContext } from '../../contexts/appContext';
import _displayLoading from '../loading/loadingComponent'
import styles from '../../Style'
import toast from '../button/Toast'


const OffreDetail = ({ route, navigation }) => {
    const { id_offre } = route.params
    const [offre, setOffre] = useState();
    const [isLoading, setIsLoading] = useState(true);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const { user } = React.useContext(AppContext)

    useEffect(() => {
        _loadOffre()
    }, [])

    const onRefresh = useCallback(() => {
        _loadOffre()

    },[])

    const _loadOffre = () => {
        const set = (data) => {
            setIsLoading(false),
                setOffre(data)
        }
        getOffreById(id_offre).then(data => set(data))
    }

    const _setFav = (id_offre) => {
        setFavOffre(id_offre, user.id_utilisateur).then(data => {
            if(data.fav)
            {
                onRefresh()
                toast("Offre ajouté aux favoris !")
            }
            else{
                onRefresh()
                toast("Offre supprimmé des favoris !")
            }
        })
    }

    const FavButton = () => {
        if (offre != undefined) {
            if (offre.favoris_offre.some(obj => obj.id_utilisateur == user.id_utilisateur)) {
                return (
                    <TouchableOpacity style={styles.headerButtonReply} onPress={() => (_setFav(offre.id_offre))}>
                        <Text><FontAwesomeIcon icon={fasHeart} size={28} style={{ color: 'white' }}></FontAwesomeIcon></Text>
                    </TouchableOpacity>
                )
            }
            else {
                return (
                    <TouchableOpacity style={styles.headerButtonReply} onPress={() => (_setFav(offre.id_offre))}>
                        <Text><FontAwesomeIcon icon={farHeart} size={28} style={{ color: 'white' }}></FontAwesomeIcon></Text>
                    </TouchableOpacity>
                )
            }
        }
        return null;
    }

    const IconHeader = () => {
        navigation.setOptions({
            title: '',
            headerRight: () => (
                <FavButton />
            )
        })

        return null
    }

    function _displayOffre() {
        if (offre !== undefined) {
            return (
                <ScrollView style={styles.offreDetailContainer} refreshControl={<RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />}>
                    <View style={styles.offreDetailHeaderContainer}>
                        <Text style={styles.offreDetailTitleText}>{offre.titre}</Text>
                    </View>
                    <View style={styles.offreDetailInformationContainer}>
                        <Text style={styles.offreDetailInformationText}><FontAwesomeIcon icon={faBuilding} size={20} /> {offre.entreprise}</Text>
                        <Text style={styles.offreDetailInformationText}><FontAwesomeIcon icon={faGraduationCap} size={20} /> {offre.niveau_etude}</Text>
                        <Text style={styles.offreDetailInformationText}><FontAwesomeIcon icon={faEuroSign} size={20} /> {offre.salaire}</Text>
                        <Text style={styles.offreDetailInformationText}><FontAwesomeIcon icon={faPhone} size={20} /> {offre.telephone}</Text>
                        <Text style={styles.offreDetailInformationText}><FontAwesomeIcon icon={faEnvelope} size={20} /> {offre.mail}</Text>
                        <Text style={styles.offreDetailInformationText}><FontAwesomeIcon icon={faCity} size={20} /> {offre.adresse} {offre.ville} {offre.code_postal}</Text>
                    </View>
                    <View style={styles.offreDetailDescriptionContainer}>
                        <Text style={styles.offreDetailDescriptionText}>{offre.description}</Text>
                    </View>
                    <View style={styles.offreDetailMapContainer}>
                    </View>
                </ScrollView>
            )
        }
        return null
    }

    return (
        <View style={styles.AppContainer}>
            <_displayOffre />
            <_displayLoading isLoading={isLoading} text={'Chargement de l\'offre '} />
            <IconHeader></IconHeader>
        </View>
    )
}

export default OffreDetail