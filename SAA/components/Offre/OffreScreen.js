import React, { useState, useCallback } from "react";
import { SafeAreaView, FlatList, View, RefreshControl, Text } from "react-native";
import { getAllOffres, setFavOffre } from "../../Api/SAA_Api";
import { AppContext } from "../../contexts/appContext";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { faFilter } from "@fortawesome/free-solid-svg-icons";

import styles from "../../Style";
import OffreItem from "./OffreItem";
import _displayLoading from "../loading/loadingComponent";
import { useFocusEffect } from "@react-navigation/native";
import toast from "../button/Toast";
import { TouchableOpacity } from "react-native-gesture-handler";

const Offre = ({ navigation }) => {
  const [offresList, setOffresList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const { user } = React.useContext(AppContext);
  const [filter, setfilter] = useState(false)
  const [color, setcolor] = useState("white");

  useFocusEffect(
    useCallback(() => {
      _loadOffres();
    }, [])
  );

  const _loadOffres = () => {
    const set = (data) => {
      setIsLoading(false), setOffresList(data), setfilter(false), setcolor('white');
    };
    getAllOffres().then((data) => set(data));
  };

  const onRefresh = useCallback(() => {
    setIsRefreshing(true);
    _loadOffres();
    setIsRefreshing(false);
  });

  const _diplayDetailForOffre = (id_offre) => {
    navigation.navigate("OffreDetail", { id_offre: id_offre });
  };

  const _setFav = (id_offre) => {
    setFavOffre(id_offre, user.id_utilisateur).then((data) => {
      if (data.fav) {
        onRefresh();
        toast("Offre ajouté aux favoris !");
      } else {
        onRefresh();
        toast("Offre supprimmé des favoris !");
      }
    });
  };

  const _filter = () => {

    if(filter)
    {
        setfilter(false);
        setcolor('white');
        _loadOffres();
    }
    else
    {
        setfilter(true);
        setcolor('#92278F');
        setOffresList(offresList.filter(e => {
            return e.favoris_offre.some(o => o.id_utilisateur == user.id_utilisateur)
        }));

    }
  }

  const IconHeader = () => {
    navigation.setOptions({
        headerRight: () => (
             <TouchableOpacity
             style={styles.headerButtonReply}
             onPress={_filter}
            >
        <Text>
          <FontAwesomeIcon
            icon={faFilter}
            size={28}
            style={{ color: color }}
          ></FontAwesomeIcon>
        </Text>
      </TouchableOpacity>
        )
    })

    return null
}

  return (
    <SafeAreaView style={styles.AppContainer}>
      <View>
        <FlatList
          data={offresList}
          keyExtractor={(item) => item.id_offre.toString()}
          renderItem={({ item }) => (
            <OffreItem
              offre={item}
              displayDetailForOffre={_diplayDetailForOffre}
              setFav={_setFav}
            />
          )}
          refreshControl={
            <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
          }
        />
      </View>
      <_displayLoading
        isLoading={isLoading}
        text="Chargement des offres"
        size="large"
      />
      <IconHeader />
    </SafeAreaView>
  );
};

export default Offre;
