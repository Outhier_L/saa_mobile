import React from 'react'
import {ScrollView, View, Text} from 'react-native'
import {getUserInfoById} from '../Api/SAA_Api'
import { AppContext } from '../contexts/appContext'
import _displayLoading from '../components/loading/loadingComponent'

import styles from '../Style'
import ButtonOpenDrawer from './button/buttonOpenDrawer'

function Profile({navigation}) {
    const [infos, setinfos] = React.useState()
    const [isLoading, setIsLoading] = React.useState(true);
    const { user } = React.useContext(AppContext)

    React.useEffect(() => {
        var mounted = true

        if(mounted)
        {
            _loadUserInfo()
        }

        return () => {
            mounted = false
        }
    }, [infos != undefined])

    const _loadUserInfo = () => {

        const set = (data) => {
            setinfos(data)
            setIsLoading(false)
        }
        getUserInfoById(user.id_utilisateur).then(data => set(data))
    }

    const DisplayRole = () => {
        if(infos != undefined)
        {
            return (
                <Text style={styles.ProfileContainerText}>Role : {infos.role.designation}</Text>
            )
        }
        return null
    }

    const DisplayInfos = () => {
        if(infos != undefined)
        {

            return (
                <View>
                    <View style={styles.ProfileInfosMainContainer}>
                        <View style={[styles.ProfileInfosContainer, styles.blueBackground]}>
                            <Text style={styles.ProfileInfosText}>Nombre d'offres créée</Text>
                            <Text style={styles.ProfileInfosTextNumber}>{infos.nbOffre}</Text>
                        </View>

                        <View style={[styles.ProfileInfosContainer, styles.yellowBackground]}>
                            <Text style={styles.ProfileInfosText}>Nombre de Forums créé</Text>
                            <Text style={styles.ProfileInfosTextNumber}>{infos.nbTopic}</Text>
                        </View>
                    </View>

                    <View style={styles.ProfileInfosMainContainer}>
                        <View style={[styles.ProfileInfosContainer, styles.blueBackground]}>
                            <Text style={styles.ProfileInfosText}>Nombre de Topic en Favoris </Text>
                            <Text style={styles.ProfileInfosTextNumber}>{infos.nbTopicFav}</Text>
                        </View>

                        <View style={[styles.ProfileInfosContainer, styles.yellowBackground]}>
                            <Text style={styles.ProfileInfosText}>Nombre de d'offre en Favoris</Text>
                            <Text style={styles.ProfileInfosTextNumber}>{infos.nbOffresFav}</Text>
                        </View>
                    </View>

                    <View style={styles.ProfileInfosMainContainer}>
                        <View style={[styles.ProfileInfosContainer, styles.purpleBackGround]}>
                            <Text style={styles.ProfileInfosText}>Nombre de Réponses données :</Text>
                            <Text style={styles.ProfileInfosTextNumber}>{infos.nbReponseTopic}</Text>
                        </View>
                    </View>

                </View>

            )
        }
        return null
    }

        return (


            <View style={styles.ProfileContainer}>
                <ScrollView>
                    <View style={styles.ProfileMainContainer}>
                            <View style={styles.ProfileHeaderContainer}>
                                <Text style={styles.ProfileHeaderText}>{user.prenom} {user.nom}</Text>
                            </View>
                            <View style={styles.ProfileContainCOntainer}>
                                <Text style={styles.ProfileContainerText}>Téléphone : {user.telephone}</Text>
                                <Text style={styles.ProfileContainerText}>Email : {user.email}</Text>
                                <DisplayRole />
                            </View>
                        </View>

                        <DisplayInfos />
                </ScrollView>
                <ButtonOpenDrawer navigation={navigation} />
                <_displayLoading isLoading={isLoading} text="Chargement des Informations de l\'utilisateur" size="large"/>
            </View>
        )
}

export default Profile