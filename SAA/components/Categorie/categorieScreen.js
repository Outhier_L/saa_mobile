import { useFocusEffect } from '@react-navigation/native';
import React, { useState, useCallback } from 'react'
import { SafeAreaView, FlatList, View} from 'react-native';
import { getAllCategories } from '../../Api/SAA_Api'

import styles from '../../Style'
import CategorieItem from '../Categorie/categorieItem'
import _displayLoading from '../loading/loadingComponent'


const Categorie = ({navigation}) => {
    const [categorieList, setCategorieList] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isRefreshing, setIsRefreshing] = useState(false);


    useFocusEffect(
        useCallback(() => {
            setIsRefreshing(true)
            _loadCategorie()
            setIsRefreshing(!setIsRefreshing)
        },[])
    )

    const onRefresh = useCallback(async() => {
        _loadCategorie()
    })

    function _loadCategorie()
    {
        const setCategorie = (data) => {
            setCategorieList(data)
            setIsLoading(false)
        }

        getAllCategories().then(data => setCategorie(data))
    }

    const _displayForum = (id_categorie) =>
    {
        navigation.navigate('Forum', {id_categorie: id_categorie})
    }

    return(
        <SafeAreaView style={styles.AppContainer}>
            <View>
                <FlatList data={categorieList} onRefresh={() => onRefresh()} refreshing={isRefreshing} keyExtractor={(item) => item.id_categorie.toString() } renderItem={({item}) => <CategorieItem categorie={item} displayForum={_displayForum}/>}/>
            </View>
            <_displayLoading isLoading={isLoading} text='Chargement des Forums' size="large"/>
        </SafeAreaView>
    )

}

export default Categorie;