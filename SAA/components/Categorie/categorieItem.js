import React from 'react'
import { View, Text, TouchableOpacity, ScrollView, Image } from 'react-native'
import moment from 'moment';
import 'moment/locale/fr';
import styles from '../../Style'
import { getPic } from '../../Api/SAA_Api'
import _displayLoading from '../loading/loadingComponent';

const CategorieItem = (props) => {
    const categorie = props.categorie
    const displayForum = props.displayForum
    moment.locale('fr')

    const DisplayLastTopic = () => {
        if (categorie.topics[0] != undefined) {
            return (
                <View style={{ flex: 8, flexWrap: 'nowrap' }}>
                    <Text>Dernier forum : {categorie.topics[0].titre}</Text>
                    <Text>      de : {categorie.topics[0].user.prenom} {categorie.topics[0].user.nom} </Text>
                    <Text>      le : {moment(categorie.topics[0].created_at).format('LL LTS')}</Text>
                </View>
            )
        }

        return null;
    }
    return (
        <ScrollView style={{ backgroundColor: '#E5E7E9' }}>
            <TouchableOpacity onPress={() => displayForum(categorie.id_categorie)} style={styles.categorieIteamMainContainer}>
                <Image source={{ uri: getPic(categorie.logo_categorie) }} style={styles.categorieItemPic}></Image>
                <View style={styles.categorieItemContent}>
                    <View style={styles.categorieItemHeaderContainer}>
                        <Text style={styles.categorieItemTitleText}>{categorie.titre_categorie}</Text>
                    </View>

                    <DisplayLastTopic />
                </View>
            </TouchableOpacity>
        </ScrollView>
    )
}

export default CategorieItem